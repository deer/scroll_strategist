# scroll\_strategist

scroll\_strategist will help you to scroll your items wisely.

Currently, the only interface to this library is
[scroll\_strategist\_cli](https://codeberg.org/deer/scroll_strategist_cli).

## How it works

### Input

scroll\_strategist takes, as its input, the following things:

- An <b><dfn>initial equipment item</dfn></b>, defined by:
    - How many <b>slots</b> the item has remaining.
    - What (relevant) <b>stats</b> the item has, represented as a [row
      vector](https://en.wikipedia.org/wiki/Row_and_column_vectors) of numbers.
      Each [index](https://en.wikipedia.org/wiki/Index_notation) (i\.e. column)
      of the vector corresponds to a certain type of stat, e\.g. WATK.
- A non[empty](https://en.wikipedia.org/wiki/Empty_set) [set][set] of scrolls,
  representing <b><dfn>the set of all kinds of scrolls that could be applied to the
  item</dfn></b>. Each scroll is defined by:
    - Its [<b>probability of
      success</b>](https://en.wikipedia.org/wiki/Bernoulli_process).
    - <b>Whether it’s a dark scroll</b>. A dark scroll with a success
      probability of &#x1d45d; has a
      &frac12;&it;(1&nbsp;&minus;&nbsp;&#x1d45d;) probability of failing in the
      way that a non-dark scroll does (doing nothing to the item other than
      removing one of its slots; we call this a “miss”), and a
      &frac12;&it;(1&nbsp;&minus;&nbsp;&#x1d45d;) probability of destroying the
      equipment item outright (we call this a “boom”).
    - The <b>cost</b> of obtaining an individual such scroll, represented as a [natural number](https://en.wikipedia.org/wiki/Natural_number) (possibly zero)<sup><a href="#fn-1-1" id="fnm-1-1">\[1\]</a></sup>. This is usually taken to be a specifically _meso_ cost, but it
      can be any measurement of “cost”, so long as the same measurement is used consistently for a given calculation.
    - What (relevant) <b>stats</b> the scroll confers to an equipment item <b>upon
      success</b>. Like the stats of an item, this is represented as a row vector
      of numbers, in which each index (i\.e. column) of the vector corresponds
      to a certain type of stat, e\.g. WATK. All scrolls must make their
      indices (viz. which indices correspond to which stats) consistent with
      the indices used by equipment item stats. For a given calculation, **<u>all stat vectors must be of the same [dimension][dim]</u>**.
- The <b><dfn>goal stats</dfn></b> (desired stats for the item), represented in the same way that item stats are represented. It’s assumed that [_every_](https://en.wikipedia.org/wiki/Logical_conjunction) index of the goal vector must be met (or exceeded) in order for the goal to be met.
- The <b><dfn>upfront cost</dfn></b> for the equipment item as it starts out. This generally includes things like the cost incurred to obtain the item (if any), the cost incurred to scroll it to its starting point (if any), etc. By default, this is zero. This can be specified for any optimisation mode (see the [“Optimisation modes”](#optimisation-modes) section below), but **<u>is only actually used by the SucExpCost mode</u>**.

Note that because of how scrolls are represented, Clean Slate Scrolls (CSS), Chaos Scrolls (CS), White Scrolls (WS), etc. cannot be represented.

### Optimisation modes

Currently, scroll\_strategist has two modes:

- <b><dfn>SucProb</dfn></b>, a\.k\.a. <b><dfn>suc-prob</dfn></b>.

  This is the default, original mode, &amp; is so named for maximising <b>suc</b>cess <b>prob</b>ability.

  This mode **<u>only</u>** attempts to maximise the [probability](https://en.wikipedia.org/wiki/Marginal_distribution) of reaching the goal, given just one (1) attempt (i\.e. one equipment item). As a result, this mode:

    - **<u>Cannot</u>** take into account scroll costs to guide its decisions, unless the
      scroll costs are needed to break a tie.
    - **<u>Cannot</u>** take into account the cost of the initial item, because that
      information is not relevant to the probability of success.
    - Will, in practice (i\.e. using actual scrolls that exist in some version of
      MapleStory), almost never tell you to use 10% scrolls, unless 30% scrolls don’t
      exist. This is because 10% scrolls and 30% scrolls confer the same stats when
      they succeed, and the 30% scroll’s thrice-as-large likelihood of succeeding
      overwhelms the downside of the 35% boom probability. The exception is when
      you’ve already reached your goal, and you have &gt;0 slots remaining, and 10%
      scrolls are the cheapest variety of scroll. In this case, the optimiser can’t
      distinguish between non-dark scrolls; the only way to fail at this point is
      to boom, so using any non-dark scroll maximises the probability of success
      (viz. to a probability of 1).
    - Will assume that failing to reach the goal is utter failure that cannot possibly be retried. This is a realistic or near-realistic assumption in some cases, but in many cases, you actually have some arbitrary number of possible attempts. In the latter cases, SucExpCost is likely a better fit.
- <b><dfn>SucExpCost</dfn></b>, a\.k\.a. <b><dfn>suc-exp-cost</dfn></b>.

  This is a newer mode, &amp; is so named for minimising the per-<b>suc</b>cess [<b>exp</b>ected](https://en.wikipedia.org/wiki/Expected_value) <b>cost</b>.

  This mode attempts to minimise the [quotient](https://en.wikipedia.org/wiki/Quotient) &#x1d450;&#x2215;&#x1d45d;, where:

    - &#x1d450; is the total overall cost, including both upfront costs &amp; scroll costs.
    - &#x1d45d; is the probability of success.

  Because 1&#x2215;&#x1d45d; is the expected number of attempts per success, &#x1d450;&#x2215;&#x1d45d; is the expected cost per success. All of this is assuming that the scroller can continue starting over from the same starting point arbitrarily many times.

  This mode makes more realistic assumptions for some practical situations, but will also often produce considerably lower success probabilities than SucProb. This is counterbalanced by lower expected costs per attempt.

### Using the optimiser

---

**&#x2139;&#xfe0f;&nbsp;See also:** The “Tips” section of the scroll\_strategist\_cli documentation.

---

Here are a few tips &amp; things to keep in mind when using scroll\_strategist.

- You usually don’t need to represent all of the equipment item’s stats, nor all of the stats possibly granted by the available scrolls. In many cases, having the stat vectors be length 1 is good enough! But the ability to specify a goal that includes more than one stat is there if you need it.
- To get more precise results where cost is involved, consider adding into your set of scrolls a non-dark “null scroll” with a success probability of 100%, a cost of zero, &amp; no stats increased upon success. scroll\_strategist\_cli can do this for you with the `--null-scroll`/`-z` [option](https://en.wikipedia.org/wiki/Command-line_interface#Command-line_option).
- Sometimes you might have stats that only indirectly affect the thing(s) that you’re actually optimising for. For example, when scrolling an overall for AVOID, overall armour for LUK scrolls are used. But these scrolls grant varying amounts of both AVOID _and_ LUK. In this example, because the [<abbr title="player character">PC</abbr>](https://en.wikipedia.org/wiki/Player_character) passively gains 0\.5 AVOID per point of LUK that they have, AVOID can instead be couched in terms of LUK. A point of AVOID is then 2 (rather than 1) of the relevant stat, and a point of LUK is 1. For example, a 30% scroll would have a stats vector (assuming that AVOID is the only thing being optimised for) of \[5&nbsp;+&nbsp;3&nbsp;&sdot;&nbsp;2\] = \[11\].

### The optimiser

---

**&#x2139;&#xfe0f;** This section was originally written for the SucProb mode. Nonetheless, most of the same principles apply to SucExpCost. The main differences are outlined in the [“SucExpCost”](#sucexpcost) section below.

---

The optimiser uses a fairly straightforward application of [dynamic
programming](https://en.wikipedia.org/wiki/Dynamic_programming) methodology.

At its core, the optimiser is imposing a certain<sup><a href="#fn-1-2" id="fnm-1-2">\[2\]</a></sup>
[semimodule](https://en.wikipedia.org/wiki/Semimodule) onto a [tree][tree] of
possible scrolling routes, where distinct subtrees are sometimes identical
(value-wise) to one another. This subtree overlap is an example of what is
called “overlapping subproblems”, in the context of dynamic programming.

The problem at hand has (again, in dynamic programming terminology) “[optimal
substructure](https://en.wikipedia.org/wiki/Optimal_substructure)”, because if
we already know the optimal scrolling strategy beyond a certain scroll usage,
then we can easily decide what scroll to use at that point. All that we have to
do is rank each possible scroll choice by a [weighted
sum](https://en.wikipedia.org/wiki/Linear_combination) of probabilities of
reaching the goal, and then pick whichever scroll ranks the highest. In the
phrase “weighted sum of probabilities of reaching the goal”, each “weight”
(coefficient) is the probability of getting that particular scroll outcome
(probability of succeeding the scroll, of missing the scroll, of booming with
the scroll), and each term is the probability of reaching your goal, assuming
that you get that particular scroll outcome, and assuming optimal scrolling
strategy from then onwards.

For example, if you have a 20% chance of reaching your goal after succeeding a 60% scroll, and a 10% chance of reaching your goal after missing a 60% scroll, then the weighted sum of probabilities of reaching the goal &mdash; given that you decide to use a 60% scroll &mdash; is (60%&nbsp;&sdot;&nbsp;20%) + (40%&nbsp;&sdot;&nbsp;10%) = 16%.

The internal logic of the optimiser is more easily seen with an example.

#### Example

Consider a average clean [Toy of
101](https://maplelegends.com/lib/equip?id=1402038), which has 3 STR, 3 DEX, 92
WATK, and 7 slots. Because we’re going to scroll for WATK, we only care about
the WATK and the STR. So, we can represent its starting stats as the row vector
&lsqb;92,&nbsp;3\], where the first column represents the WATK, and the second column
represents the STR. Then, the set of scrolls that we want to consider using
includes 10% scrolls, 30% scrolls, 60% scrolls, 70% scrolls, and 100% scrolls
(all for two-handed sword for WATK). And finally, let’s say that our goal is
&lsqb;108,&nbsp;10\] &mdash; in other words, at least 108 WATK, _**<u>and</u>**_ at least 10 STR.

[scroll\_strategist\_cli](https://codeberg.org/deer/scroll_strategist_cli) (a
frontend for scroll\_strategist) accepts
[JSON](https://en.wikipedia.org/wiki/JSON) input in a certain format. Using
said format, we can represent the input like so (the costs are rough estimates
of free-market meso costs in MapleLegends as of this writing):

```json
{
  "slots": 7,
  "stats": [92, 3],
  "scrolls": [
    { "percent":  10, "dark": false, "cost":  100000, "stats": [5, 3] },
    { "percent":  30, "dark": true,  "cost": 1300000, "stats": [5, 3] },
    { "percent":  60, "dark": false, "cost":   50000, "stats": [2, 1] },
    { "percent":  70, "dark": true,  "cost":   35000, "stats": [2, 1] },
    { "percent": 100, "dark": false, "cost":   70000, "stats": [1, 0] }
  ],
  "goal": [108, 10]
}
```

With this input, the optimiser will then perform a [depth-first search (<abbr title="depth-first search">DFS</abbr>)][dfs] of the tree mentioned above. This tree is constructed in
[memory](https://en.wikipedia.org/wiki/Computer_data_storage#Primary_storage) _only as needed_, and
subtrees that are known to be suboptimal are pruned as they are discovered. The
diagrams below illustrate a (small portion of a) pruned tree:

<details>
<summary>Legend</summary>

![Legend for the diagram below](legend.svg "Legend for the diagram below")

</details>

![Example illustration of a small portion of a pruned tree](example_tree.svg "Example illustration of a small portion of a pruned tree")

Within the “scroll usage” (hexagonal) nodes:

- “<b>P&af;(goal)</b>” refers to the probability of reaching the goal, given that this
  scroll is chosen, but **<u>not</u>** assuming any particular outcome of the scroll.
- “<b>E&af;\[cost\]</b>” refers to the
  [expected](https://en.wikipedia.org/wiki/Expected_value) cost (due _solely_
  to scroll expenditure) incurred due to this scroll being used, in addition to
  all future scrolls used. Costs of scrolls that were already used in the past
  are not considered.

As you’d expect, the scroll that the optimiser chooses to use at any given
point is just the scroll that yields [the
largest](https://en.wikipedia.org/wiki/Maxima_and_minima) associated
“<b>P&af;(goal)</b>”.

#### DFS

Notice that the choice of specifically _<abbr title="depth-first search">DFS</abbr>_ is very significant. Because we make a scroll choice based on the ensuing probability of reaching the goal, we can only make that choice after already making all possible subsequent scroll choices. Without making those future scroll choices, we don’t know what the probability of success is!

Of course, this has to stop somewhere. We need to know how we’ll deal with the future in order to act now, but we can’t look indefinitely far forward, because that would take an indefinite amount of [calculation time](https://en.wikipedia.org/wiki/Time_complexity). Luckily, it _does_ stop somewhere: typically, the equipment item runs out of slots, or it booms.

By performing a DFS, we drill “down” (read: away from the [root]) to these terminal states ASAP. Once we start hitting terminals &mdash; that is, leaf nodes &mdash; we can propagate the relevant probabilities upwards, forming scrolling decisions as we go. This “prunes” the tree, because for any node that is a scroll usage, we prune all of them except for the one that’s determined to be optimal.

#### Optimising the optimiser

As we move across the breadth (as opposed to depth) of the tree, we begin to notice that certain distinct subtrees are value-wise
identical (“overlapping subproblems”). Thus, the most important optimisation to this
DFS is simply keeping a [memo
(cache)](https://en.wikipedia.org/wiki/Memoization). This cache
[maps](https://en.wikipedia.org/wiki/Hash_table) item states (slots &amp; stats) to
their corresponding subtrees, so that those subtrees never have to be
re-generated/searched again. Thus, only one copy of each subtree is kept in memory. This is a _massive_ [time complexity](https://en.wikipedia.org/wiki/Time_complexity) reduction, and probably also a massive [space complexity](https://en.wikipedia.org/wiki/Space_complexity) reduction as well.

Furthermore, another (smaller) performance optimisation is the so-called
“master scroll” optimisation. The master scroll optimisation generates a
“master scroll” before initiating the DFS. The master scroll is a _purely theoretical_ scroll that has a 100% probability of success, and grants a bonus
to each stat that is equal to the highest bonus granted to that stat by any of
the scrolls in the input set<sup><a href="#fn-1-3" id="fnm-1-3">\[3\]</a></sup>. When considering what scroll to
use next, we can first check whether or not we would reach our goal by using
the master scroll on all remaining slots. If the answer is “no, the goal would
still not be reached”, then we know that reaching the goal is simply
impossible, and we can give up right away. This results in a different kind of leaf node, &amp; one that is closer to the root than its unoptimised counterpart would be.

#### SucExpCost

The SucExpCost mode uses most of the same optimisation principles as the SucProb mode, with the primary exception being its criteria for choosing between multiple possible scroll choices.

Whereas SucProb chooses whichever scroll results in the largest success probability, SucExpCost chooses the scroll that minimises &#x1d450;&#x2215;&#x1d45d;, where:

- &#x1d450; is the **<u>total overall</u>** cost, including both upfront costs &amp; scroll costs.
- &#x1d45d; is the probability of success.

Because &#x1d450; is the total overall cost, it has to include [sunk costs](https://en.wikipedia.org/wiki/Sunk_cost). This might seem unintuitive, because the [bygones principle](https://en.wikipedia.org/wiki/Sunk_cost#Bygones_principle) tells us that sunk costs are not relevant to the decisions that we make now &amp; in the future &mdash; the antithesis of this principle being the [sunk cost _fallacy_](https://en.wikipedia.org/wiki/Sunk_cost#Fallacy_effect).

But when performing DFS (see the [“DFS”](#dfs) section above), we’re really testing _possible_ scrolling routes &mdash; we haven’t _actually_ committed to any decisions yet. This is because DFS, perhaps somewhat unintuitively, is effectively working backwards: it drills down to leaf nodes (where scrolling is forced to stop), &amp; then works its way upwards (towards the [root], i\.e. towards the initial equipment item state), committing to scroll choices as it goes.

Taking sunk cost into account thus puts all branches of the tree “on the same page”, so to speak. This prevents the DFS from committing to late scroll choices based _only_ on later scroll choices, when those late scroll choices actually implicitly lock it into suboptimal earlier choices that would produce large sunk costs.

However, taking sunk cost into account also means that our “item state” nodes of the tree look a bit different, because they have to store that sunk cost. Moreover, when looking into the cache (see the [“Optimising the optimiser”](#optimising-the-optimiser) section above), the keys into the cache are just those item states, meaning that the sunk cost is now part of the key; this potentially lowers the cache hit rate, but in practice, the hit rate is still quite high.

It’s also reasonable to worry that &#x1d450;&#x2215;&#x1d45d; is frequently [undefined, because &#x1d45d; is zero](https://en.wikipedia.org/wiki/Division_by_zero). However, this isn’t really an issue, because when &#x1d45d; is zero, we give up anyway! So when &#x1d45d;&nbsp;=&nbsp;0, we can treat &#x1d450;&#x2215;&#x1d45d; as being [at least as large as any other possible value](https://en.wikipedia.org/wiki/Infimum_and_supremum) of &#x1d450;&#x2215;&#x1d45d;; that is, effectively [+&infin;](https://en.wikipedia.org/wiki/Extended_real_number_line). If we’re comparing two values of &#x1d450;&#x2215;&#x1d45d; where both &#x1d45d;s are zero, then we can instead just compare the &#x1d450; values directly.

## Using scroll\_strategist directly

scroll\_strategist is a [Rust](https://www.rust-lang.org/) [library][lib] that exposes one [function][fun] for each optimisation mode (see the [“Optimisation modes”](#optimisation-modes) section above).

For example, to use the SucProb mode, you might write something like:

```rust
use scroll_strategist::{dfs, graph::ItemState, scroll::Scroll, stats};

let slots = 7;
let stats = stats![92, 3];
let stats_dim = stats.len();
let mut item_state = ItemState::<()>::new_exists(slots, stats);
let scrolls = [
    Scroll::null(stats_dim),
    Scroll::new(0.1, false,   100_000, stats![5, 3]),
    Scroll::new(0.3, true,  1_300_000, stats![5, 3]),
    Scroll::new(0.6, false,    40_000, stats![2, 1]),
    Scroll::new(0.7, true,     45_000, stats![2, 1]),
    Scroll::new(1.0, false,    70_000, stats![1, 0]),
];
let goal = stats![108, 10];
dfs::solve(&mut item_state, &scrolls, &goal);
```

The entirely-solved tree is now contained in `item_state`. The tree can be inspected by, for example, doing something like this:

```rust
if let ItemState::Exists {
    slots,
    stats,
    sunk_cost,
    child: Some(child),
} = item_state
{
    /* … */
}
```

To use SucExpCost, let the `C` type parameter be `u32` instead of `()`, and use `dfs_c::solve_c` instead of `dfs::solve`.

[lib]: https://en.wikipedia.org/wiki/Library_(computing)
[fun]: https://en.wikipedia.org/wiki/Function_(computer_programming)

## Probably unimportant implementation details

You may notice that `ItemState` contains an [`Rc`](https://doc.rust-lang.org/alloc/rc/struct.Rc.html), as it’s part of the public API (`ItemState::Exists.child`). This is because, due to the caching (see the [“Optimising the optimiser”](#optimising-the-optimiser) section above), the nodes of the tree don’t necessarily reference only their children; they can reference arbitrary subtrees elsewhere in the graph. `Rc` allows this model to be implemented safely &amp; easily. It’s probably(?) possible to circumvent the use of reference counting entirely by simply allocating the graph objects in an [arena](https://en.wikipedia.org/wiki/Region-based_memory_management) or something like that, but the implementation would be far messier, &amp; likely so too would the API.

## Legal

[<abbr title="GNU Affero General Public License">AGPL</abbr> <abbr title="version">v</abbr>3](https://www.gnu.org/licenses/agpl-3.0)+

---

## Footnotes for “scroll\_strategist”

1. <a href="#fnm-1-1" id="fn-1-1">\[↑\]</a> Internally represented as an [unsigned 32-bit integer][int-types], meaning that costs are specifically elements of the set [\[][interval]0,&nbsp;2<sup>32</sup>[)][interval]&nbsp;[&cap;][intersection]&nbsp;[&integers;](https://en.wikipedia.org/wiki/Integer).
2. <a href="#fnm-1-2" id="fn-1-2">\[↑\]</a> We’ll call the [semiring](https://en.wikipedia.org/wiki/Semiring)
   underlying this semimodule “&#x1d445;”. &#x1d445; has, as its underlying set, the [unit interval (&#x2110;)](https://en.wikipedia.org/wiki/Unit_interval), because its members are probabilities (of getting a certain outcome after using a given scroll, or of reaching the goal). Its additive [operation](https://en.wikipedia.org/wiki/Binary_operation) is the “[&#x1d5c6;&#x1d5ba;&#x1d5d1;](https://en.wikipedia.org/wiki/Maxima_and_minima)” operation, which takes in two [real numbers](https://en.wikipedia.org/wiki/Real_number), and yields the larger (the <b>max</b>imum) of the two numbers. Its multiplicative operation is just ordinary multiplication of real numbers (&#x2217;). So then, our semiring &#x1d445; is:

   &#x1d445; &#x225d; (&#x2110;, &#x1d5c6;&#x1d5ba;&#x1d5d1;, &#x2217;).

   To show that this is in fact a semiring, we can prove that it satisfies the
   semiring axioms ([for
   any](https://en.wikipedia.org/wiki/Universal_quantification) &#x1d44e;, &#x1d44f;, &#x1d450; &in; &#x2110;):

   <details>
   <summary>Proof that &#x1d445; is a semiring</summary>

   - (&#x2110;, &#x1d5c6;&#x1d5ba;&#x1d5d1;) is an
     [abelian](https://en.wikipedia.org/wiki/Commutative_property)
     [monoid](https://en.wikipedia.org/wiki/Monoid) with an
     [identity](https://en.wikipedia.org/wiki/Identity_element) called 0:
       - &#x2110; is [closed][closure] under &#x1d5c6;&#x1d5ba;&#x1d5d1;. The larger of two elements of &#x2110; has to
         be… an element of &#x2110;.
       - &#x1d5c6;&#x1d5ba;&#x1d5d1; is
         [associative](https://en.wikipedia.org/wiki/Associative_property):
         &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44e;, &#x1d44f;}, &#x1d450;} = &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44e;, &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44f;, &#x1d450;}} = &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44e;, &#x1d44f;, &#x1d450;}.
       - &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44e;, 0} = &#x1d44e; for any &#x1d44e; &in; &#x2110;, because 0 is the unique minimum
         element of &#x2110; &mdash; anything is at least as large as 0. So 0 is the identity
         of &#x1d5c6;&#x1d5ba;&#x1d5d1;.
       - &#x1d5c6;&#x1d5ba;&#x1d5d1; is commutative: &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44e;, &#x1d44f;} = &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44f;, &#x1d44e;}.
   - (&#x2110;, &#x2217;) is a monoid with an identity called 1:
       - &#x2110; is [closed][closure] under &#x2217;. Multiplying two numbers in the unit
         interval can never yield a number greater than 1, as multiplying by a
         number in the unit interval can’t make a number larger &mdash; at best, you can
         just not change its value (by multiplying by 1). Similarly, multiplying
         two numbers in the unit interval can never yield a number less than 0, as
         none of the elements of the unit interval are
         [negative](https://en.wikipedia.org/wiki/Negative_number).
       - &#x2217; is [associative](https://en.wikipedia.org/wiki/Associative_property);
         it inherits this property from ordinary multiplication of real numbers.
       - 1 is the identity element: 1 &#x2217; &#x1d44e; = &#x1d44e; = &#x1d44e; &#x2217; 1.
       - &#x2217; is commutative; it inherits this property from ordinary multiplication
         of real numbers.
   - &#x2217; [left-distributes and
     right-distributes](https://en.wikipedia.org/wiki/Distributive_property) over
     &#x1d5c6;&#x1d5ba;&#x1d5d1;:
       - &#x1d44e; &#x2217; &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44f;, &#x1d450;} = &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44e; &#x2217; &#x1d44f;, &#x1d44e; &#x2217; &#x1d450;}. Multiplying by a larger
         nonnegative real number always gets you a larger result.
       - &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44e;, &#x1d44f;} &#x2217; &#x1d450; = &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44e; &#x2217; &#x1d450;, &#x1d44f; &#x2217; &#x1d450;}. This follows from the
         above, combined with the commutativity of &#x2217;.
   - Multiplication by 0
     [annihilates](https://en.wikipedia.org/wiki/Absorbing_element) &#x1d445;:
     0 &#x2217; &#x1d44e; = 0 = &#x1d44e; &#x2217; 0.

   </details>

   And &#x1d445; really is just a semiring, and not a [ring][ring], because inverse
   elements for &#x1d5c6;&#x1d5ba;&#x1d5d1; are not possible.

   To build a semimodule on top of our semiring &#x1d445;, we introduce an abelian monoid
   &#x1d440;:

   &#x1d440; &#x225d; (&#x2110;<sup>3</sup>, &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>3</sup>).

   &#x2110;<sup>3</sup> is just
   &#x2110; [&times;](https://en.wikipedia.org/wiki/Cartesian_product) &#x2110; &times; &#x2110;, i\.e. a
   [triple](https://en.wikipedia.org/wiki/Tuple) (&#x1d44e;, &#x1d44f;, &#x1d450;) for some
   &#x1d44e;, &#x1d44f;, &#x1d450; &in; &#x2110;. Alternatively, we can write each element as a row vector:
   &lsqb;&#x1d44e;, &#x1d44f;, &#x1d450;\]. And &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>3</sup> is just &#x1d5c6;&#x1d5ba;&#x1d5d1; applied component-wise:

   &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>3</sup>&af;{\[&#x1d44e;, &#x1d44f;, &#x1d450;\], &lsqb;&#x1d451;, &#x1d452;, &#x1d453;\]} &#x225d;
   &lsqb;&#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44e;, &#x1d451;}, &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44f;, &#x1d452;}, &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d450;, &#x1d453;}\].

   Then, we introduce the “[scalar
   multiplication](https://en.wikipedia.org/wiki/Scalar_multiplication)” operation (&sdot;):

   &#x1d44e; &sdot; &lsqb;&#x1d44f;, &#x1d450;, &#x1d451;\] &#x225d; &lsqb;&#x1d44e; &#x2217; &#x1d44f;, &#x1d44e; &#x2217; &#x1d450;, &#x1d44e; &#x2217; &#x1d451;\].

   This means that (&sdot;) is defined more or less identically to scalar
   multiplication over &reals;<sup>3</sup>.

   Then, our semimodule &#x1d446;<sub>&#x1d445;</sub> is just:

   &#x1d446;<sub>&#x1d445;</sub> &#x225d; (&#x1d445;, &#x1d440;, &sdot;).

   To prove that this really is a semimodule, we can show that it satisfies the
   semimodule axioms (for any &#x1d463;, &#x1d464; &in; &#x1d440;):

   <details>
   <summary>Proof that &#x1d446;<sub>&#x1d445;</sub> is a semimodule</summary>

   - &#x1d44e; &sdot; &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>3</sup>&af;{&#x1d463;, &#x1d464;} = &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>3</sup>&af;{&#x1d44e; &sdot; &#x1d463;, &#x1d44e; &sdot; &#x1d464;}. This
     follows from the combination of &#x1d445; being a semiring (particularly the
     distributive property), with both &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>3</sup> and (&sdot;) being defined
     component-wise.
   - &#x1d5c6;&#x1d5ba;&#x1d5d1;&af;{&#x1d44e;, &#x1d44f;} &sdot; &#x1d463; = &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>3</sup>&af;{&#x1d44e; &sdot; &#x1d463;, &#x1d44f; &sdot; &#x1d463;}. This also follows
     from the combination of &#x1d445; being a semiring (particularly the distributive
     property), with both &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>3</sup> and (&sdot;) being defined component-wise.
   - (&#x1d44e; &#x2217; &#x1d44f;) &sdot; &#x1d463; = &#x1d44e; &sdot; (&#x1d44f; &sdot; &#x1d463;). This follows from the associativity of &#x2217;,
     and the fact that (&sdot;) is defined in terms of &#x2217;.
   - 1 &sdot; &#x1d463; = &#x1d463;. This follows from 1 being the identity element of &#x2217;.
   - 0<sub>&#x1d445;</sub> &sdot; &#x1d463; = &#x1d44e; &sdot; 0<sub>&#x1d440;</sub> = 0<sub>&#x1d440;</sub> = &lsqb;0, 0, 0\].
     This follows from 0 being the annihilating element of &#x2217;.

   (As written, these are actually the axioms that &#x1d446;<sub>&#x1d445;</sub> must satisfy to
   be a **<u>left</u>** &#x1d445;-semimodule. But you can see that it’s also a right
   &#x1d445;-semimodule, due to all operations involved being commutative.)

   </details>

   The whole reason why we want the &#x1d5c6;&#x1d5ba;&#x1d5d1; (and thus also &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>3</sup>)
   operation is because this is the operation that we apply to choose between one
   or more competing strategies. Whichever one maximises the probability of
   reaching the goal is the chosen strategy. And our &#x2217; (and thus also &sdot;) operation
   corresponds to the fact that, for any two [independent][independence]
   [events][event] &#x1d452;<sub>1</sub> [&Vbar;][independence] &#x1d452;<sub>2</sub>,

   [&#x1d5af;][p]&af;(&#x1d452;<sub>1</sub> [&cap;][intersection] &#x1d452;<sub>2</sub>) =
   &#x1d5af;&af;(&#x1d452;<sub>1</sub>) &#x2217; &#x1d5af;&af;(&#x1d452;<sub>2</sub>).

   Independence is guaranteed by the fact that every scroll application is a
   totally independent event &mdash; independent of everything, unless you happen to
   know the internal state of the [server](https://en.wikipedia.org/wiki/Game_server)’s
   [PRNG](https://en.wikipedia.org/wiki/Pseudorandom_number_generator)…!

   Using a given scroll yields three possible outcomes (technically, not all are
   necessarily “possible”; some may have probability 0, [so long as the sum of all
   three is 1](https://en.wikipedia.org/wiki/Unit_measure)):

   - Success.
   - Miss.
   - Boom.

   Each one of these outcomes yields some equip as a result (or no equip at all,
   in the case of a boom), and we can look ahead to see what the probability is of
   reaching our goal, given some yielded equip (and given the optimal scrolling
   strategy). So, each one of these outcomes has two probabilities attached to it:
   the probability that the outcome happens at all (e\.g. 40% for a “miss” of a
   60% scroll), and the probability of reaching the goal, given that the outcome
   does happen. We can aggregate the outcome probabilities into an element
   &#x1d45c; &in; &#x2110;<sup>3</sup> (e\.g. &#x1d45c; = &lsqb;0\.3, 0\.35, 0\.35\] for a 30% scroll), and
   the corresponding goal-reaching probabilities into another element
   &#x1d454; &in; &#x2110;<sup>3</sup>. Then, the overall probability of reaching the goal by using
   this scroll is just the [dot
   product](https://en.wikipedia.org/wiki/Dot_product) &lang;&#x1d45c;, &#x1d454;&rang; of these two
   vectors:

   &lang;&#x1d45c;, &#x1d454;&rang; &#x225d; (&#x1d45c;<sub>1</sub> &#x2217; &#x1d454;<sub>1</sub>) + (&#x1d45c;<sub>2</sub> &#x2217; &#x1d454;<sub>2</sub>) + (&#x1d45c;<sub>3</sub> &#x2217; &#x1d454;<sub>3</sub>).

   This yields an element of &#x1d445; (i\.e. an element of &#x2110;), which we can then do more
   semiring-y things with. Do note that when defining &lang;&#x25a1;, &#x25b3;&rang;, we’re using ordinary
   addition of real numbers, _not_ &#x1d5c6;&#x1d5ba;&#x1d5d1;. This is because we don’t have a choice
   in which scroll outcome we get; we can only decide which scroll to use, and
   RNGsus does the rest. So, the best that we can do is calculate an
   [expectation](https://en.wikipedia.org/wiki/Expected_value).

   You might reasonably wonder what the &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>3</sup> operation is used for,
   or what it signifies. In this case, it’s not actually important; it has no
   particular meaning (but also see <sup><a href="#fn-1-3">\[3\]</a></sup> for the application of this
   operation to stat vectors). However, it is both:

   - A natural component-wise extension of &#x1d5c6;&#x1d5ba;&#x1d5d1;,
   - _and_ sufficient to make &#x1d440; an abelian monoid, and &#x1d446;<sub>&#x1d445;</sub> a semimodule.

   The operation that we _actually_ want to get out of &#x1d446;<sub>&#x1d445;</sub> being a
   semimodule is (&sdot;), i\.e. scalar multiplication. As mentioned above, our &#x2217; (and
   thus also &sdot;) operation corresponds to the [conjunction][intersection] of
   independent events. In particular, for some
   &#x1d41e; = &lsqb;&#x1d452;<sub>1</sub>, &#x1d452;<sub>2</sub>, &#x1d452;<sub>3</sub>\], and some event
   &#x1d453; &Vbar; &#x1d41e;, we have:

   &lsqb;&#x1d5af;&af;(&#x1d453; &cap; &#x1d452;<sub>1</sub>), &#x1d5af;&af;(&#x1d453; &cap; &#x1d452;<sub>2</sub>), &#x1d5af;&af;(&#x1d453; &cap; &#x1d452;<sub>3</sub>)\] =
   &#x1d5af;&af;(&#x1d453; &cap; &#x1d41e;) = &#x1d5af;&af;(&#x1d453;) &sdot; &#x1d5af;&af;(&#x1d41e;).

   This equation lets us obtain, for example, the probabilities of getting the
   various scroll outcomes at a certain scroll choice within our scrolling
   strategy (e\.g. &lsqb;passing, missing, booming\] a certain 30% scroll), before we
   even start scrolling. If we know the probability of getting to that point in
   the strategy (call this probability &#x1d5af;&af;(&#x1d453;)), then we can just scalar-multiply
   that probability by the scroll’s outcome probability vector (in this case,
   &lsqb;30%, 35%, 35%\], because it’s a 30% scroll). The result is then:

   &lsqb;&#x1d5af;&af;(&#x1d453;) &#x2217; 30%, &#x1d5af;&af;(&#x1d453;) &#x2217; 35%, &#x1d5af;&af;(&#x1d453;) &#x2217; 35%\].

   And of course, as mentioned before, the “inner product” (dot product) of two
   such vectors is crucial to the operation of the optimiser.
3. <a href="#fnm-1-3" id="fn-1-3">\[↑\]</a> Think &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>3</sup>, but more like &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>&#x1d45b;</sup>, where &#x1d45b; is
   the number of columns/indices in the stat vectors. The stat vector of the
   master scroll is &#x1d5c6;&#x1d5ba;&#x1d5d1;<sup>&#x1d45b;</sup> of the stat vectors of all of the scrolls
   in the input set.

[set]: https://en.wikipedia.org/wiki/Set_(mathematics)
[tree]: https://en.wikipedia.org/wiki/Tree_(graph_theory)
[dfs]: https://en.wikipedia.org/wiki/Depth-first_search
[closure]: https://en.wikipedia.org/wiki/Closure_(mathematics)
[ring]: https://en.wikipedia.org/wiki/Ring_(mathematics)
[intersection]: https://en.wikipedia.org/wiki/Intersection_(set_theory)
[independence]: https://en.wikipedia.org/wiki/Independence_(probability_theory)
[event]: https://en.wikipedia.org/wiki/Event_(probability_theory)
[p]: https://en.wikipedia.org/wiki/Probability
[int-types]: https://en.wikipedia.org/wiki/Integer_(computer_science)#Common_integral_data_types
[interval]: https://en.wikipedia.org/wiki/Interval_(mathematics)
[dim]: https://en.wikipedia.org/wiki/Matrix_(mathematics)#Size
[root]: https://en.wikipedia.org/wiki/Tree_(graph_theory)#Rooted_tree

---
