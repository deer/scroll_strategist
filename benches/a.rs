use divan::{black_box, black_box_drop, Bencher};

fn main() {
    divan::main();
}

#[divan::bench]
fn toy_of_101(bencher: Bencher) {
    use scroll_strategist::{
        dfs::solve, graph::ItemState, scroll::Scroll, stats,
    };

    let mut init_state = ItemState::<()>::new_exists(7, stats![96, 3, 3, 0]);
    let scrolls = [
        Scroll::new(0.1, false, 100_000, stats![5, 3, 0, 1]),
        Scroll::new(0.3, true, 1_300_000, stats![5, 3, 0, 1]),
        Scroll::new(0.6, false, 40_000, stats![2, 1, 0, 0]),
        Scroll::new(0.7, true, 45_000, stats![2, 1, 0, 0]),
        Scroll::new(1.0, false, 70_000, stats![1, 0, 0, 0]),
    ];

    bencher.bench_local(|| {
        black_box(solve(&mut init_state, &scrolls, &stats![111, 0, 0, 0]))
    });

    black_box_drop(init_state);
}

#[divan::bench]
fn zhelm(bencher: Bencher) {
    use scroll_strategist::{
        dfs_c::solve_c, graph::ItemState, scroll::Scroll, stats,
    };

    let mut init_state = ItemState::<u32>::new_exists(10, stats![17], 0);
    let scrolls = [
        Scroll::null(1),
        Scroll::new(0.3, true, 2_000_000, stats![3]),
        Scroll::new(0.6, false, 7_000_000, stats![2]),
        Scroll::new(0.7, true, 3_500_000, stats![2]),
        Scroll::new(1.0, false, 3_200_000, stats![1]),
    ];

    bencher.bench_local(|| {
        black_box(solve_c(&mut init_state, &scrolls, &stats![35]))
    });

    black_box_drop(init_state);
}
