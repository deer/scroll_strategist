use crate::stats::Stats;
use std::hash::{Hash, Hasher};

/// This type exists specifically to avoid calling
/// [`SmallVec::clone`](smallvec::SmallVec::clone) every time that we do a
/// lookup in the cache.
pub(crate) enum StatsHandle<'sh> {
    Owned(Stats),
    Borrowed(&'sh Stats),
}

impl<'sh> PartialEq for StatsHandle<'sh> {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Owned(s0), Self::Owned(s1)) => s0 == s1,
            (Self::Owned(s0), Self::Borrowed(s1)) => &s0 == s1,
            (Self::Borrowed(s0), Self::Owned(s1)) => s0 == &s1,
            (Self::Borrowed(s0), Self::Borrowed(s1)) => s0 == s1,
        }
    }
}

impl<'sh> Eq for StatsHandle<'sh> {}

impl<'sh> Hash for StatsHandle<'sh> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            Self::Owned(s) => s.hash(state),
            Self::Borrowed(s) => s.hash(state),
        }
    }
}

/// This type exists specifically to avoid calling
/// [`SmallVec::clone`](smallvec::SmallVec::clone) every time that we do a
/// lookup in the cache.
pub(crate) struct CacheKey<'sh, C> {
    slots: u8,
    stats: StatsHandle<'sh>,
    sunk_cost: C,
}

impl<'sh> CacheKey<'sh, ()> {
    pub fn new_owned(slots: u8, stats: Stats) -> Self {
        Self {
            slots,
            stats: StatsHandle::Owned(stats),
            sunk_cost: (),
        }
    }

    pub fn new_borrowed(slots: u8, stats: &'sh Stats) -> Self {
        Self {
            slots,
            stats: StatsHandle::Borrowed(stats),
            sunk_cost: (),
        }
    }
}
impl<'sh> CacheKey<'sh, u32> {
    pub fn new_owned(slots: u8, stats: Stats, sunk_cost: u32) -> Self {
        Self {
            slots,
            stats: StatsHandle::Owned(stats),
            sunk_cost,
        }
    }

    pub fn new_borrowed(slots: u8, stats: &'sh Stats, sunk_cost: u32) -> Self {
        Self {
            slots,
            stats: StatsHandle::Borrowed(stats),
            sunk_cost,
        }
    }
}

impl<'sh> PartialEq for CacheKey<'sh, ()> {
    fn eq(&self, other: &Self) -> bool {
        self.slots == other.slots && self.stats == other.stats
    }
}
impl<'sh> PartialEq for CacheKey<'sh, u32> {
    fn eq(&self, other: &Self) -> bool {
        self.slots == other.slots
            && self.stats == other.stats
            && self.sunk_cost == other.sunk_cost
    }
}

impl<'sh, C> Eq for CacheKey<'sh, C> where CacheKey<'sh, C>: PartialEq {}

impl<'sh, C: Hash> Hash for CacheKey<'sh, C> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.slots.hash(state);
        self.stats.hash(state);
        self.sunk_cost.hash(state);
    }
}
