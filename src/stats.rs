use smallvec::SmallVec;
use std::{
    cmp::Ordering,
    fmt,
    hash::{Hash, Hasher},
    iter::FromIterator,
    ops::Mul,
};

/// An ordered array of stats that an item can have (see
/// [`ItemState`](crate::graph::ItemState)), or that a scroll can grant on
/// success (see [`Scroll`](crate::scroll::Scroll)).
///
/// The elements of the stat array are not identified nominally, and can only
/// be identified by their position/index within the array. As such, obviously,
/// care must be taken when inserting elements into, & when copying to/from,
/// the stats array. Particularly, the exact indices of the elements in a given
/// stat array must be respected.
///
/// When performing operations that take two different `Stats` structs, like
/// testing for equality, [`Stats::max_in_place`], etc., it is assumed that the
/// two `Stats` structs have stat arrays of equal length. This invariant is
/// only checked using debug assertions; in release mode, breaking this
/// invariant might still panic, or might even silently fail!
#[derive(Clone, Debug)]
pub struct Stats {
    stats: SmallVec<[u16; 4]>,
}

impl Stats {
    /// Creates a new `Stats` using the provided stat array. This method should
    /// only be used for convenience, or if you already have a
    /// [`Vec<u16>`](Vec) anyway for some reason. The internal representation
    /// of a `Stats` is [`SmallVec<[u16; 4]>`](SmallVec), so you can instead
    /// just call [`.into()`](Into) on a `SmallVec<[u16; 4]>`, or use the more
    /// generic [`Stats::from_iter`] if appropriate.
    pub fn from_vec(stats: Vec<u16>) -> Self {
        Self {
            stats: SmallVec::from_vec(stats),
        }
    }

    /// The length of this `Stats`'s stat array.
    pub fn len(&self) -> usize {
        self.stats.len()
    }

    /// Returns `self.len() == 0`.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Adds `self` to `other`, using ordinary addition, and returns the result
    /// as a freshly-allocated `Stats`.
    ///
    /// ## Invariants:
    ///
    /// - `self.len() == other.len()`
    pub fn plus(&self, other: &Self) -> Self {
        debug_assert_eq!(self.len(), other.len());

        Self {
            stats: self
                .stats
                .iter()
                .zip(other.stats.iter())
                .map(|(s0, s1)| s0 + s1)
                .collect(),
        }
    }

    /// Performs summation of `self` with `other`, over the [max
    /// tropical](https://en.wikipedia.org/wiki/Tropical_semiring)
    /// [semimodule](https://en.wikipedia.org/wiki/Semimodule), but the result
    /// is simply used to mutate `self` in-place.
    ///
    /// This function is used by
    /// [`Scroll::master_scroll`](crate::scroll::Scroll::master_scroll).
    ///
    /// ## Invariants:
    ///
    /// - `self.len() == other.len()`
    pub fn max_in_place(&mut self, other: &Stats) {
        debug_assert_eq!(self.len(), other.len());

        for (i, stat) in other.stats.iter().enumerate() {
            if stat > &self.stats[i] {
                self.stats[i] = *stat;
            }
        }
    }
}

impl FromIterator<u16> for Stats {
    /// Creates a new `Stats` with the stats yielded by the iterator `iter`.
    fn from_iter<I: IntoIterator<Item = u16>>(iter: I) -> Self {
        Self {
            stats: iter.into_iter().collect(),
        }
    }
}

impl From<SmallVec<[u16; 4]>> for Stats {
    fn from(stats: SmallVec<[u16; 4]>) -> Self {
        Self { stats }
    }
}

impl PartialEq for Stats {
    /// ## Invariants:
    ///
    /// - `self.len() == other.len()`
    fn eq(&self, other: &Self) -> bool {
        debug_assert_eq!(self.len(), other.len());

        self.stats == other.stats
    }
}

impl Eq for Stats {}

impl Hash for Stats {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.stats.hash(state);
    }
}

impl PartialOrd for Stats {
    /// Returns `None` any time that `self.len() != other.len()`. Returns
    /// `None` any time that one or more stats are _larger_ in `self` (compared
    /// to the same stats in `other`), but there are also one or more stats
    /// that are _smaller_ in `self`.
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.len() != other.len() {
            return None;
        }

        let mut part_ord = None;

        for ord in self
            .stats
            .iter()
            .zip(other.stats.iter())
            .map(|(s0, s1)| s0.cmp(s1))
        {
            match part_ord {
                None => part_ord = Some(ord),
                Some(Ordering::Less) => {
                    if ord == Ordering::Greater {
                        return None;
                    }
                }
                Some(Ordering::Equal) => {
                    if ord != Ordering::Equal {
                        part_ord = Some(ord);
                    }
                }
                Some(Ordering::Greater) => {
                    if ord == Ordering::Less {
                        return None;
                    }
                }
            }
        }

        part_ord
    }
}

impl Mul<u16> for Stats {
    type Output = Stats;

    /// Implements [scalar
    /// multiplication](https://en.wikipedia.org/wiki/Scalar_multiplication).
    fn mul(mut self, rhs: u16) -> Self::Output {
        for stat in self.stats.iter_mut() {
            *stat *= rhs;
        }

        self
    }
}

impl fmt::Display for Stats {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut first = true;
        for stat in self.stats.iter() {
            if first {
                write!(f, "{stat}")?;
                first = false;
            } else {
                write!(f, ", {stat}")?;
            }
        }

        Ok(())
    }
}
