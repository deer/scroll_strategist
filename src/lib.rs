#![doc = include_str!("../README.md")]
#![forbid(unsafe_code)]
#![deny(clippy::all)]
#![deny(deprecated)]

mod cache;
pub mod dfs;
pub mod dfs_c;
pub mod graph;
pub mod scroll;
mod stats;

pub use smallvec;
pub use stats::Stats;

/// Preciously thin wrapper around [`smallvec::smallvec`]. Heh...
#[macro_export]
macro_rules! stats {
    ($elem:expr; $n:expr) => ({
        $crate::Stats::from($crate::smallvec::smallvec![$elem; $n])
    });
    ($($x:expr),*$(,)*) => ({
        $crate::Stats::from($crate::smallvec::smallvec![$($x),*])
    });
}

#[test]
fn toy_of_101_test() {
    use crate::{dfs::solve, graph::ItemState, scroll::Scroll, stats::Stats};

    let mut init_state = ItemState::<()>::new_exists(7, stats![96, 3, 3, 0]);
    let scrolls = [
        Scroll::new(0.1, false, 100_000, stats![5, 3, 0, 1]),
        Scroll::new(0.3, true, 1_300_000, stats![5, 3, 0, 1]),
        Scroll::new(0.6, false, 40_000, stats![2, 1, 0, 0]),
        Scroll::new(0.7, true, 45_000, stats![2, 1, 0, 0]),
        Scroll::new(1.0, false, 70_000, stats![1, 0, 0, 0]),
    ];

    solve(&mut init_state, &scrolls, &stats![111, 0, 0, 0]);

    if let ItemState::Exists {
        slots: _,
        stats: _,
        sunk_cost: _,
        child,
    } = init_state
    {
        let child = child.unwrap();

        assert_eq!(
            child.scroll(),
            &Scroll::new(0.3, true, 1_300_000, Stats::from_iter([5, 3, 0, 1])),
        );
        assert_eq!(child.p_goal, 0.186_686_606_25);
        assert_eq!(child.exp_cost, 2_871_256.933_75);
    }
}
