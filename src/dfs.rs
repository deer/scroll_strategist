use crate::{
    cache::CacheKey,
    graph::{ItemState, ScrollUse},
    scroll::Scroll,
    stats::Stats,
};
use rustc_hash::FxHashMap;
use std::{cmp::Ordering, rc::Rc};

/// Corresponds to ProbSuc.
///
/// Like other search functions in this program, this function assumes that
/// `state` already has a well-defined value for `state.slots` and
/// `state.stats`. Also, if `state.child.is_some()`, the value inside of
/// `state.child` _will_ be ignored, and trampled/replaced. `scrolls` must be
/// nonempty.
///
/// ## Optimisation mode
///
/// This function optimises _only_ to maximise the probability of reaching
/// `goal`, going with lower expected costs only when needed to break a tie.
pub fn solve<'a>(
    state: &mut ItemState<'a, ()>,
    scrolls: &'a [Scroll],
    goal: &Stats,
) {
    let master_scroll = Scroll::master_scroll(scrolls);
    let mut cache = Default::default();

    dfs(state, scrolls, &master_scroll, goal, &mut cache);
}

/// Like other search functions in this program, this function assumes that
/// `state` already has a well-defined value for `state.slots` and
/// `state.stats`. Also, if `state.child.is_some()`, the value inside of
/// `state.child` _will_ be ignored, and trampled/replaced. `scrolls` must be
/// nonempty.
///
/// ## Optimisation mode
///
/// This version of DFS optimises _only_ to maximise the probability of
/// reaching `goal`, going with lower expected costs only when needed to break
/// a tie.
///
/// ## Additional parameters
///
/// The `master_scroll` parameter is used solely for optimisation, i.e. it's
/// not _strictly_ necessary to have the correct master scroll for this
/// function to behave correctly. However, **if you don't want to supply the
/// correct master scroll, you should supply a null scroll** (any scroll that
/// doesn't change the item on success) to preserve correctness.
///
/// Likewise, the `cache` parameter is also used solely for [dynamic
/// programming](https://en.wikipedia.org/wiki/Dynamic_programming)
/// optimisation. If no particular value for `cache` is appropriate, just pass
/// in the empty map.
///
/// ## Returns
///
/// - Probability of reaching `goal`, assuming optimal scroll choices after
///   this point.
/// - Expected cost after this point, again assuming optimal scroll choices
///   after this point.
fn dfs<'a>(
    state: &mut ItemState<'a, ()>,
    scrolls: &'a [Scroll],
    master_scroll: &Scroll,
    goal: &Stats,
    cache: &mut FxHashMap<CacheKey<()>, Rc<ScrollUse<'a, ()>>>,
) -> (f64, f64) {
    debug_assert!(!scrolls.is_empty());

    match state {
        ItemState::Exists {
            slots,
            stats,
            sunk_cost: _,
            child,
        } => {
            // Check the cache for whether or not we have already optimised
            // from this starting point before. If so, we can return without
            // doing any real work.
            if let Some(su) =
                cache.get(&CacheKey::<()>::new_borrowed(*slots, stats))
            {
                child.replace(Rc::clone(su));

                return (su.p_goal, su.exp_cost);
            }

            // Just in case `child.is_some()`.
            let _ = child.take();

            if *slots == 0 {
                return (if &*stats >= goal { 1.0 } else { 0.0 }, 0.0);
            }

            let slots_m1 = *slots - 1;

            let mut best_su: Option<ScrollUse<'_, _>> = None;
            for scroll in scrolls {
                let mut scroll_use = ScrollUse::new(scroll);

                if scroll.p_suc > 0.0 {
                    // New stats of the item, assuming a success of this
                    // scroll.
                    let outcome_suc_stats = stats.plus(&scroll.stats);

                    // Is it even possible to reach the goal at this point?
                    // This is the "master scroll" heuristic.
                    match (outcome_suc_stats.plus(
                        &(master_scroll.stats.clone() * u16::from(slots_m1)),
                    ))
                    .partial_cmp(goal)
                    {
                        Some(Ordering::Less) | None => continue,
                        _ => (),
                    }

                    let outcome_suc = scroll_use.outcomes_mut().set_pass(
                        ItemState::<()>::new_exists(
                            slots_m1,
                            outcome_suc_stats,
                        ),
                    );

                    // (probability of reaching the goal conditioned on this
                    // scroll succeeding, expected cost after this point
                    // conditioned on this scroll succeeding)
                    let (p_goal_cond_suc, exp_cost_cond_suc) =
                        dfs(outcome_suc, scrolls, master_scroll, goal, cache);
                    scroll_use.p_goal += scroll.p_suc * p_goal_cond_suc;
                    scroll_use.exp_cost += scroll.p_suc * exp_cost_cond_suc;
                }

                // Is it even possible to reach the goal, assuming that this
                // scroll fails? This is the "master scroll" heuristic.
                let goal_possible_cond_fail = &(stats.plus(
                    &(master_scroll.stats.clone() * u16::from(slots_m1)),
                )) >= goal;

                if goal_possible_cond_fail && scroll.p_suc < 1.0 {
                    let outcome_fail = scroll_use.outcomes_mut().set_miss(
                        ItemState::<()>::new_exists(slots_m1, stats.clone()),
                    );

                    // (probability of reaching the goal conditioned on this
                    // scroll failing, expected cost after this point
                    // conditioned on this scroll failing)
                    let (p_goal_cond_fail, exp_cost_cond_fail) =
                        dfs(outcome_fail, scrolls, master_scroll, goal, cache);
                    let p_fail = if scroll.dark {
                        (1.0 - scroll.p_suc) / 2.0
                    } else {
                        1.0 - scroll.p_suc
                    };
                    scroll_use.p_goal += p_fail * p_goal_cond_fail;
                    scroll_use.exp_cost += p_fail * exp_cost_cond_fail;

                    if scroll.dark {
                        let outcome_boom = scroll_use
                            .outcomes_mut()
                            .set_boom(ItemState::new_boomed());

                        // These results are always `(0.0, 0.0)`, so we ignore
                        // them.
                        let (_p_goal_cond_boom, _exp_cost_cond_boom) = dfs(
                            outcome_boom,
                            scrolls,
                            master_scroll,
                            goal,
                            cache,
                        );
                    }
                }

                // Now, we check whether or not using this scroll is a better
                // choice than using any of the scrolls that we tested
                // previously.
                let replace = best_su.as_ref().map_or(true, |child_su|
                    // We use the expected cost to break ties here.
                    scroll_use.p_goal > child_su.p_goal
                        || (scroll_use.p_goal >= child_su.p_goal
                            && scroll_use.exp_cost < child_su.exp_cost));
                if replace {
                    best_su = Some(scroll_use);
                }
            }
            if let Some(su) = best_su {
                child.replace(Rc::new(su));
            }

            if let Some(child_scroll_use) = child.as_ref() {
                cache.insert(
                    CacheKey::<()>::new_owned(*slots, stats.clone()),
                    Rc::clone(child_scroll_use),
                );

                (child_scroll_use.p_goal, child_scroll_use.exp_cost)
            } else {
                // This branch is possibly taken when our "master scroll"
                // heuristic rejects all scrolls.
                (0.0, 0.0)
            }
        }
        ItemState::Boomed => (0.0, 0.0),
    }
}
