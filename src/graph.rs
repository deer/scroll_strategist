use crate::{scroll::Scroll, stats::Stats};
use std::rc::Rc;

/// The state of an equipment item, including:
///
/// - how many slots it has left,
/// - what its stats are,
/// - & how much cost (of type `C`) has been sunk into the item so far.
///
/// This last element (sunk cost) includes upfront cost, plus costs incurred by
/// scrolls that have already been used.
///
/// This is a node of a scrolling strategy tree, so it also can have (or may
/// not have) a single "child" of type [`ScrollUse`]. This only supports one
/// child at most, because we only want to keep the _optimal_ scroll usage in
/// memory; others should be, and are, discarded.
///
/// This is an enum because we want a way to represent the "state" of an item
/// that no longer exists, because it was boomed by a dark scroll.
pub enum ItemState<'a, C> {
    Exists {
        slots: u8,
        stats: Stats,
        sunk_cost: C,
        child: Option<Rc<ScrollUse<'a, C>>>,
    },
    Boomed,
}

impl<'a, C> ItemState<'a, C> {
    /// Creates a new instance of this type, specifically of the
    /// `ItemState::Boomed` variant. This contains no useful information other
    /// than that the item was boomed.
    pub const fn new_boomed() -> Self {
        Self::Boomed
    }
}
impl<'a> ItemState<'a, ()> {
    /// Creates a new instance of this type, specifically of the
    /// `ItemState::Exists` variant. The child is defaulted to `None`.
    pub const fn new_exists(slots: u8, stats: Stats) -> Self {
        Self::Exists {
            slots,
            stats,
            sunk_cost: (),
            child: None,
        }
    }
}
impl<'a> ItemState<'a, u32> {
    /// Creates a new instance of this type, specifically of the
    /// `ItemState::Exists` variant. The child is defaulted to `None`.
    pub const fn new_exists(slots: u8, stats: Stats, sunk_cost: u32) -> Self {
        Self::Exists {
            slots,
            stats,
            sunk_cost,
            child: None,
        }
    }
}

/// An instance of a particular scroll being used on a particular
/// [`ItemState`]. Like [`ItemState`], this type represents a part of a
/// scrolling strategy tree; in particular, this represents a different kind of
/// node than an [`ItemState`] does.
///
/// The child nodes here are themselves [`ItemState`]s, representing all
/// possible outcomes of this scroll usage. The outcomes are stored in what is
/// effectively a [`Vec`].
///
/// This struct contains a member representing the probability of reaching the
/// goal given that this scroll is chosen (but not assuming any particular
/// outcome of the scroll). There is also a member of this struct representing
/// the expected cost (due solely to scroll expenditure) incurred due to this
/// scroll being used, in addition to all future scrolls used. The future
/// scroll costs are calculated optimally, as usual.
pub struct ScrollUse<'a, C> {
    /// "Probability of goal": Represents the probability of reaching the goal
    /// given that this scroll is chosen (but not assuming any particular
    /// outcome of the scroll).
    pub p_goal: f64,
    /// "Expected cost": Represents the expected cost (due solely to scroll
    /// expenditure) incurred due to this scroll being used, in addition to all
    /// future scrolls used. The future scroll costs are calculated optimally,
    /// as usual.
    pub exp_cost: f64,
    /// The scroll being used.
    scroll: &'a Scroll,
    /// All possible outcomes of this scroll usage; the children of this node.
    outcomes: Outcomes<'a, C>,
}

impl<'a, C> ScrollUse<'a, C> {
    /// Creates a new scroll usage struct, given a particular scroll that is
    /// being used. The probability of reaching the goal defaults to zero, the
    /// expected cost defaults to the cost of `scroll`, and there are no
    /// outcomes/children.
    pub fn new(scroll: &'a Scroll) -> Self {
        Self {
            p_goal: 0.0,
            exp_cost: f64::from(scroll.cost),
            scroll,
            outcomes: Outcomes::new(),
        }
    }

    /// Returns a reference to the scroll being used here.
    pub const fn scroll(&self) -> &'a Scroll {
        self.scroll
    }

    /// Returns a reference to the set of all possible outcomes.
    pub fn outcomes(&self) -> &Outcomes<'a, C> {
        &self.outcomes
    }

    /// Returns a mutable reference to the set of all possible outcomes.
    pub fn outcomes_mut(&mut self) -> &mut Outcomes<'a, C> {
        &mut self.outcomes
    }
}

/// All possible outcomes of a particular scroll usage. Each outcome is
/// represented as an [`ItemState`]. See the documentation for [`ScrollUse`]
/// (and for [`ItemState`]) for more info.
#[derive(Default)]
pub struct Outcomes<'a, C> {
    pass: Option<ItemState<'a, C>>,
    miss: Option<ItemState<'a, C>>,
    boom: Option<ItemState<'a, C>>,
}

impl<'a, C> Outcomes<'a, C> {
    /// Creates a new empty set of outcomes.
    pub fn new() -> Self {
        Self {
            pass: None,
            miss: None,
            boom: None,
        }
    }

    /// Replaces the existing "pass" outcome, if any, & returns a mutable
    /// reference to the newly added outcome.
    pub fn set_pass(
        &mut self,
        pass_outcome: ItemState<'a, C>,
    ) -> &mut ItemState<'a, C> {
        self.pass.insert(pass_outcome)
    }

    /// Replaces the existing "miss" outcome, if any, & returns a mutable
    /// reference to the newly added outcome.
    pub fn set_miss(
        &mut self,
        miss_outcome: ItemState<'a, C>,
    ) -> &mut ItemState<'a, C> {
        self.miss.insert(miss_outcome)
    }

    /// Replaces the existing "boom" outcome, if any, & returns a mutable
    /// reference to the newly added outcome.
    pub fn set_boom(
        &mut self,
        boom_outcome: ItemState<'a, C>,
    ) -> &mut ItemState<'a, C> {
        self.boom.insert(boom_outcome)
    }

    /// Returns a reference to the "pass" outcome, if any.
    pub fn pass(&self) -> Option<&ItemState<'a, C>> {
        self.pass.as_ref()
    }

    /// Returns a reference to the "miss" outcome, if any.
    pub fn miss(&self) -> Option<&ItemState<'a, C>> {
        self.miss.as_ref()
    }

    /// Returns a reference to the "boom" outcome, if any.
    pub fn boom(&self) -> Option<&ItemState<'a, C>> {
        self.boom.as_ref()
    }
}
